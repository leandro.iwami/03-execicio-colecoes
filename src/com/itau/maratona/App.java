package com.itau.maratona;

import java.util.List;

public class App {
	
	public static void main(String[] args) {
		Arquivo arquivo = new Arquivo("src/alunos.csv");
		List<Aluno> alunos = arquivo.ler();
		
		List<Equipe> equipes = CriadorEquipes.construir(alunos, 3);

		Impressora.imprimir(equipes);
	}
}
