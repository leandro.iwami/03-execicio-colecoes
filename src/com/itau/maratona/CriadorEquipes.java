package com.itau.maratona;

import java.util.ArrayList;
import java.util.List;

public class CriadorEquipes {
	
	public static List<Equipe> construir(List<Aluno> listaAlunos, int quantidade){
		List<Equipe> listaEquipes = new ArrayList<>();
		Equipe equipe = new Equipe();
		equipe.id = 1;
		int idAtual = 2;
		
		while(!listaAlunos.isEmpty()) {
			if(equipe.alunos.size() == quantidade) {
				listaEquipes.add(equipe);
				
				equipe = new Equipe();
				equipe.id = idAtual;
				idAtual++;
			}
			
			int posicao = sortearAluno(listaAlunos.size());
			Aluno aluno = listaAlunos.remove(posicao);
			equipe.alunos.add(aluno);
		}
		
		
		listaEquipes.add(equipe);
		
		
		return listaEquipes;
	}
	
	private static int sortearAluno(int tamanho) {
		Double sorteio = Math.floor(Math.random() * tamanho);
		
		return sorteio.intValue();
	}
}
